// global variables.
var showInConsole = true;
var registering = false;
var stage = "";
var Administrador = unsafeWindow.Administrador;
var Utilidades = unsafeWindow.Utilidades;
var $ = unsafeWindow.$;
var Tabla = unsafeWindow.Tabla;
var CONSTANTES = unsafeWindow.CONSTANTES;
var n = {};
var especial = false;
var dias = [];
var antesDe = 0;
var despuesDe = 0;

function showLog(msg) {
    if (showInConsole) console.log(msg);
}

Utilidades.LlamadaAjaxPostJsonAsync = function (i, r, t) {
    n = Utilidades;
    if (!t) t = 18e4;
    var u = !1,
        f;
    try {
        return window.navigator.onLine ? (f = (new Date).getTime(), n.mostrarAnimacionCarga(), $.ajax({
            type: "POST",
            url: i.url,
            dataType: r ? r : "json",
            data: i.datos,
            timeout: t,
            success: function (t) {
                var u = (new Date).getTime(),
                    e = u - f,
                    r = !0;
                t && t.error ? (alertify.error(t.error), Administrador.recargarCaptcha(), n.ocultarAnimacionCarga()) : i.funcion && (i.parametrosAdicionalesFuncion = i.parametrosAdicionalesFuncion ? i.parametrosAdicionalesFuncion : [], i.parametrosAdicionalesFuncion.unshift(t), r = i.funcion.apply(this, i.parametrosAdicionalesFuncion));
                r || n.ocultarAnimacionCarga()
            },
            async: !0,
            cache: !1,
            error: function (t, r, f) {
                n.ocultarAnimacionCarga();
                falloMatricula();
                f === "timeout" ? (alertify.error(CONSTANTES.Mensajes["999"]), alertify.okBtn("No").cancelBtn("Sí").confirm("¿Desea volver a intentar?", function (n) {
                    n.preventDefault();
                    u = !0
                }, function (t) {
                    return t.preventDefault(), n.LlamadaAjaxPostJsonAsync(i)
                })) : i.funcionError && (i.parametrosAdicionalesError = i.parametrosAdicionalesError ? i.parametrosAdicionalesError : [], i.parametrosAdicionalesError.unshift(t), i.parametrosAdicionalesError.unshift(r), i.parametrosAdicionalesError.unshift(f), i.funcionError.apply(this, i.parametrosAdicionalesError))
            }
        })) : alertify.okBtn("No").cancelBtn("Sí").confirm("¿Desea volver a intentar? <\/br> No hay conexión a internet.", function (n) {
            n.preventDefault();
            u = !0
        }, function (t) {
            return t.preventDefault(), n.LlamadaAjaxPostJsonAsync(i)
        }), u
    } catch {
        Utilidades.LlamadaAjaxPostJsonAsync(i, r, t);
    }
};

function startMatricula() {
    try {
        showLog("iniciando again");
        stage = "0 buscando citas";
        var n, t, i;
        Administrador.usuario.oficina != undefined && Administrador.usuario.oficina != null && Administrador.usuario.oficina !== "" ?
            (Administrador.usuario.oficina = $("#listaSedes li.seleccionado").attr("id"), Utilidades.eliminarErrorDiv("Oficina"),
                n = $("#oficinaSeleccionada"), t = $("#sedeSeleccionada"), n.length > 0 && t.text(n.text()), i = {
                    usuarioObjeto: JSON.stringify(Administrador.usuario)
                },
                Utilidades.LlamadaAjaxPostJsonAsync({
                    funcion: ct,
                    url: "/MatriculaTeorica/cargarCuposDisponibles",
                    datos: i
                }, undefined, 20e4)) : (Utilidades.agregarErrorDiv("Oficina", CONSTANTES.Mensajes["007"]), console.log("error al iniciar, posiblemente por falta de oficina."));
    }
    catch {
        startMatricula();
    }
}

function ct(t) {
    stage = "respuesta cita";
    if (registering) return;
    try {
        if (t[t.length - 1] != undefined && t[t.length - 1].OficinaTexto != undefined && (Administrador.usuario.oficinaTexto = t[t.length - 1].OficinaTexto), t[0] != undefined && t[0].Codigo != undefined) {
            Utilidades.agregarErrorDiv("CD", t[0].Descripcion);
            falloMatricula();
        } else {
            registering = true;
            stage = "cita encontrada";
            if (Administrador.irArriba(), t.pop(), Utilidades.eliminarErrorDiv("CD"), n.tablaCitas !== undefined && n.tablaCitas !== null) {
                var i = $(".ctrl-tabla").parent();
                n.tablaCitas.destruirTabla();
                $("<div class='col-sm-12 ctrl-tabla ctrl-tabla-prueba-teorica-minimo-ancho'><\/div>").appendTo(i)
            }
            n.tablaCitas = new Tabla({
                contenedor: $(".ctrl-tabla"),
                datos: st(t),
                columnas: ht(t),
                tipoSeleccion: "celda",
                eventoSeleccionCelda: lt,
                columnaNoSeleccionable: "Fecha",
                filtro: !1
            });
            $("#divPCitas").collapse("show");
            $("#divPDatos").collapse("hide");
            $("#divPSedes").collapse("hide");
            $("#divPResumen").collapse("hide");
            $("#divPMatricula").collapse("hide");
            Utilidades.limpiarErrorDiv();
            seleccionarCita(t);
        }
    } catch {
        registering = false;
        startMatricula();
    }
}

function seleccionarCita(t) {
    stage = "seleccionando cita";
    var d = t[0];
    Administrador.usuario.fechaCita = d.Fecha;
    Administrador.usuario.horaCita = d.Hora;
    $("#fechaCita").text(Administrador.usuario.fechaCita);
    $("#horaCita").text(Administrador.usuario.horaCita);
    $("#divCitaSeleccionada").collapse("show");
    showLog("seleccionando cita");
    reservarCupo();
}

function reservarCupo() {
    stage = "llamando api reserva cupo";
    try {
        Utilidades.eliminarErrorDiv("cargaResumenMatricula");
        Utilidades.LlamadaAjaxPostJsonAsync({
            funcion: ni,
            url: "/MatriculaTeorica/ReservarCupoCitaTeorica",
            datos: {
                usuarioObjeto: JSON.stringify(Administrador.usuario)
            }
        })
    } catch {
        falloMatricula();
    }
}

function ni(n) {
    stage = "respuesta reserva cupo";
    try {
        if (Utilidades.limpiarErrorDiv(), n[n.length - 1] != undefined && n[n.length - 1].Codigo != undefined && n[n.length - 1].Codigo !== "00") {
            var t = n[n.length - 1].Codigo,
                i = n[n.length - 1].Descripcion;
            n.pop();
            t === "2" ? (c(), h(), alertify.alert("Usuario ya cuenta con matrícula para prueba teórica."), setTimeout(function () {
                window.location.replace("/Formularios/MatriculaPruebaTeorica")
            }, 4e3)) : t === "1" ? (n[0] != undefined && n[0].Codigo != undefined && n[0].Codigo === "27" ? falloMatricula() : n[0] != undefined && n[0].Codigo != undefined ? falloMatricula() : p(n, !0, !0), seleccionarCita()) : (Utilidades.agregarErrorDiv("RCT", i), Administrador.irAbajo(), falloMatricula())
        } else n[1] != undefined && (Administrador.irArriba(), Utilidades.eliminarErrorDiv("RCT"), $("#fechaCita").text(Administrador.usuario.fechaCita), $("#horaCita").text(Administrador.usuario.horaCita), $("#divCitaSeleccionada").collapse("show"), Administrador.usuario.tipoCursoTexto = n[1].tipocurso, Administrador.usuario.aula = n[1].aula, Administrador.usuario.otrasSenas = n[1].otras_senas, Utilidades.limpiarErrorDiv(), matricular())
    } catch {
        falloMatricula();
    }
}

function matricular() {
    showLog("matriculando");
    stage = "iniciando matricula";
    try {
        Utilidades.LlamadaAjaxPostJsonAsync({
            funcion: ii,
            funcionError: ti,
            url: "/MatriculaTeorica/matricularPrueba",
            datos: {
                usuarioObjeto: JSON.stringify(Administrador.usuario)
            }
        });
    } catch {
        falloMatricula();
    }
}

function ii(n) {
    stage = "respuesta matricula"
    try{
    n.Error != undefined && n.Error.Codigo !== "00" ? n.Error.Codigo === "20" ? (c(), h(), alertify.alert("Usuario ya cuenta con matrícula para prueba teórica."), setTimeout(function () {
        window.location.replace("/Formularios/MatriculaPruebaTeorica")
    }, 4e3)) : (Utilidades.agregarErrorDiv("MATR", n.Error.Descripcion), Administrador.irAbajo(), l(!1, !0), falloMatricula() ) : (Administrador.irArriba(), Utilidades.eliminarErrorDiv("MATR"), n.CURSO_TEORICO != undefined ? ($("#nombreMatricula").text(Administrador.usuario.nombre), $("#tipoIdeMatricula").text(Administrador.usuario.tipoIdentificacionTexto), $("#identificacionMatricula").text(Administrador.usuario.identificacion), $("#cursoMatricula").text(Administrador.usuario.tipoCursoTexto), Administrador.usuario.cursoRegularTexto != undefined && Administrador.usuario.cursoRegularTexto != null && Administrador.usuario.cursoRegularTexto != "" ? ($("#cursoRegularMatricula").text(Administrador.usuario.cursoRegularTexto), Administrador.usuario.tipoCurso === "7" ? $("#cursoRegularEtiquetaMatricula").text("Curso Transporte Público:") : $("#cursoRegularEtiquetaMatricula").text("Curso Regular:"), $("#divCursoRegularMatricula").collapse("show")) : ($("#cursoRegularMatricula").text(""), $("#divCursoRegularMatricula").collapse("hide")), $("#oficinaMatricula").text(Administrador.usuario.oficinaTexto), $("#fechaMatricula").text(Administrador.usuario.fechaCita), $("#horaMatricula").text(Administrador.usuario.horaCita), $("#aulaMatricula").text(Administrador.usuario.aula), n.CURSO_TEORICO.Descripcion === undefined || n.CURSO_TEORICO.Descripcion === null || n.CURSO_TEORICO.Descripcion === "" ? $("#otrasIndMatricula").text("-") : $("#otrasIndMatricula").text(n.CURSO_TEORICO.Descripcion), $("#divPMatricula").collapse("show"), $("#divPDatos").collapse("hide"), h(), $("#divPCitas").collapse("hide"), $("#divPSedes").collapse("hide"), $("#divPResumen").collapse("hide"), Utilidades.limpiarErrorDiv(), Administrador.usuario.identificacion = null, Administrador.usuario.llave = null) : Utilidades.agregarErrorDiv("MATR", CONSTANTES.Mensajes["009"]), falloMatricula())
    } catch {

        falloMatricula();
    }
}

function falloMatricula() {
    showLog("matricula llego a etapa " + stage + " pero fallo. Recomenzando el ciclo.");
    registering = false;
    startMatricula();
}




// funciones requeridas segundarias
function p(n, t, r) {
    var f, e, o;
    n[n.length - 1] != undefined && n[n.length - 1].OficinaTexto != undefined && (Administrador.usuario.oficinaTexto = n[n.length - 1].OficinaTexto);
    t && c();
    n[0] != undefined && n[0].Codigo != undefined ? (Utilidades.agregarErrorDiv("CD", n[0].Descripcion), Administrador.irAbajo(), r ? (c(), $("#divPSedes").collapse("show"), $("#divPDatos").collapse("hide"), h(), $("#divPCitas").collapse("hide"), $("#divPResumen").collapse("hide"), $("#divPMatricula").collapse("hide")) : l(!0, !0), Administrador.usuario.fechaCita == null && Administrador.usuario.horaCita == null && Utilidades.eliminarErrorDiv("RCT")) : (Administrador.irArriba(), n.pop(), Utilidades.eliminarErrorDiv("CD"), Utilidades.eliminarErrorDiv("RCT"), f = dt(n), i.tablaCitas !== undefined && i.tablaCitas !== null ? (e = $(".ctrl-tabla").parent(), i.tablaCitas.destruirTabla(), $("<div class='col-sm-12 ctrl-tabla' style='min-width:" + u + "px'><\/div>").appendTo(e)) : o = $(".ctrl-tabla").css("min-width", u + "px"), i.tablaCitas = new Tabla({
        contenedor: $(".ctrl-tabla"),
        datos: kt(n),
        columnas: f,
        tipoSeleccion: "celda",
        eventoSeleccionCelda: gt,
        columnaNoSeleccionable: "Fecha",
        filtro: !1
    }), l(!0, !0), $("#divPCitas").collapse("show"), $("#divPDatos").collapse("hide"), $("#divPSedes").collapse("hide"), $("#divPResumen").collapse("hide"), $("#divPMatricula").collapse("hide"), Utilidades.limpiarErrorDiv())
}

function ti() {
    l(!1, !0)
}

function c() {
    Administrador.usuario.fechaCita = null;
    Administrador.usuario.horaCita = null;
    $("#divCitaSeleccionada").collapse("hide");
    $("#horaCita").text("");
    $("#fechaCita").text("")
}

function h() {
    clearInterval(n);
    n = 0;
    $("#divPRelojContadorPT").collapse("hide")
}

function lt(n) {
    n != undefined && n.codigoColumna != undefined && n.codigoColumna != null && n.fila != undefined && n.fila != null && n.fila.Fecha != undefined && n.fila.Fecha != null && (Administrador.usuario.fechaCita = n.fila.Fecha, Administrador.usuario.horaCita = n.codigoColumna, $("#fechaCita").text(Administrador.usuario.fechaCita), $("#horaCita").text(Administrador.usuario.horaCita), $("#divCitaSeleccionada").collapse("show"))
}

function ht(n) {
    var r = [],
        t = [],
        i, e, u, f, o;
    for (i in n) {
        if (!n.hasOwnProperty(i)) break;
        t.indexOf(n[i].Hora) === -1 && t.push(n[i].Hora)
    }
    t.sort();
    e = 85 / t.length;
    for (u in t) {
        if (!t.hasOwnProperty(u)) break;
        f = t[u];
        o = {
            codigo: f,
            nombre: f,
            ancho: e + "%"
        };
        r.push(o)
    }
    return r.unshift({
        codigo: "Fecha",
        nombre: "Fecha",
        ancho: "15%"
    }), r;
}

function st(n) {
    var r = [],
        e = [],
        t, u, f, i;
    for (t in n) {
        if (!n.hasOwnProperty(t)) break;
        if (e.indexOf(n[t].Fecha) === -1) u = n[t].Hora, i = {
            Fecha: n[t].Fecha
        }, i[u] = n[t].Disponibles, e.push(n[t].Fecha), r.push(i);
        else {
            u = n[t].Hora;
            f = {};
            for (i in r) r[i].Fecha === n[t].Fecha && (f = r[i]);
            f[u] = n[t].Disponibles
        }
    }
    return r
}

setTimeout(function () {
    showLog("sustituyendo evento");
    var a = $("#botonContinuarSedes");
    a.click(startMatricula);
}, 6000);