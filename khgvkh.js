console.log("hola desde CDN 3.0");

// global variables.
	var showInConsole = true;
	var registering = false;
    var stage = "";
    var contRequest= 0;
	var Administrador = unsafeWindow.Administrador;
	var Utilidades = unsafeWindow.Utilidades;
	var $ = unsafeWindow.$;
	var Tabla = unsafeWindow.Tabla;
    var CONSTANTES = unsafeWindow.CONSTANTES;
    var n = {};
    var indicatedTime = false;
    var mainExecuted = false;

    function principalRequest(){
        if(!mainExecuted){
        mainExecuted = true;
        
        var now = new Date();
        var later = new Date();
        if (now.getMinutes() <= 30) {
            later.setMinutes(30);
            later.setSeconds(0);
            later.setMilliseconds(0);
            var remaining = (later.getTime() - now.getTime());
            remaining = remaining + Math.floor(Math.random() * (2 - -1) + -1);
            console.log(remaining + " tiempo remaining1");
            setTimeout(function() {indicatedTime = true; startMatricula();}, remaining);
        } else {
            if(now.getHours == 23){
                later.setHours(0);
            } else {
                later.setHours(now.getHours() + 1);
            }
            later.setMinutes(0);
            later.setSeconds(0);
            later.setMilliseconds(0);
            var remaining = (later.getTime() - now.getTime());
            remaining = remaining + Math.floor(Math.random() * (2 - -1) + -1);
            console.log(remaining + " tiempo remaining2");
            setTimeout(function() {indicatedTime = true; startMatricula();}, remaining);
        }
        setTimeout(function() {
            mainExecuted = false;
        }, remaining + 1200000);
        setTimeout(function() {
            indicatedTime = false;
        }, remaining + 120000);
    }
       
    }
    function repeter() {
        var i = setInterval(function() {startMatricula(); console.log("repeter");}, 3000);
        setTimeout(function(){clearInterval(i)}, 12000);
    }

    function start() {
        setInterval(function(){startMatricula()}, 180000);
        principalRequest();
        setInterval(function(){principalRequest()}, 59000);
    }

	function showLog(msg) {
		if (showInConsole) console.log(msg);
	}

	Utilidades.LlamadaAjaxPostJsonAsync = function (i, r, t) {
		var n = Utilidades;
		if (!t) t = 150000;
		var u = !1,
			f;
		try {
			return window.navigator.onLine ? (f = (new Date).getTime(), n.mostrarAnimacionCarga(), $.ajax({
				type: "POST",
				url: i.url,
				dataType: r ? r : "json",
				data: i.datos,
				timeout: t,
				success: function (t) {
					var u = (new Date).getTime(),
						e = u - f,
						r = !0;
					t && t.error ? (alertify.error(t.error), Administrador.recargarCaptcha(), n.ocultarAnimacionCarga()) : i.funcion && (i.parametrosAdicionalesFuncion = i.parametrosAdicionalesFuncion ? i.parametrosAdicionalesFuncion : [], i.parametrosAdicionalesFuncion.unshift(t), r = i.funcion.apply(this, i.parametrosAdicionalesFuncion));
					r || n.ocultarAnimacionCarga()
				},
				async: !0,
				cache: !1,
				error: function (t, r, f) {
					n.ocultarAnimacionCarga();
					falloMatricula();
					f === "timeout" ? (alertify.error(CONSTANTES.Mensajes["999"]), alertify.okBtn("No").cancelBtn("Sí").confirm("¿Desea volver a intentar?", function (n) {
						n.preventDefault();
						u = !0
					}, function (t) {
						return t.preventDefault(), n.LlamadaAjaxPostJsonAsync(i)
					})) : i.funcionError && (i.parametrosAdicionalesError = i.parametrosAdicionalesError ? i.parametrosAdicionalesError : [], i.parametrosAdicionalesError.unshift(t), i.parametrosAdicionalesError.unshift(r), i.parametrosAdicionalesError.unshift(f), i.funcionError.apply(this, i.parametrosAdicionalesError))
				}
			})) : alertify.okBtn("No").cancelBtn("Sí").confirm("¿Desea volver a intentar? <\/br> No hay conexión a internet.", function (n) {
				n.preventDefault();
				u = !0
			}, function (t) {
				return t.preventDefault(), n.LlamadaAjaxPostJsonAsync(i)
			}), u
		} catch {
			Utilidades.LlamadaAjaxPostJsonAsync(i, r, t);
		}
    };

    function startMatricula() {
        contRequest++;
        showLog("iniciando again");

        if (Administrador.usuario.oficina != undefined && Administrador.usuario.oficina != null && Administrador.usuario.oficina !== "") {
            Administrador.usuario.oficina = $("#listaSedes li.seleccionado").attr("id");
            Utilidades.eliminarErrorDiv("Oficina");
            var n = $("#oficinaSeleccionada"),
                t = $("#sedeSeleccionada");
            n.length > 0 && t.text(n.text());
            Utilidades.LlamadaAjaxPostJsonAsync({
                parametrosAdicionalesFuncion: [!0, !0],
                funcion: f,
                url: "/MatriculaPractica/CargarCuposDisponibles",
                datos: {
                    usuarioObjeto: JSON.stringify(Administrador.usuario),
                    rutasObjeto: JSON.stringify(Administrador.rutas)
                }
            })
        } else Utilidades.agregarErrorDiv("Oficina", CONSTANTES.Mensajes["007"])

    }

    function f(n, i, r) {
        var t = Utilidades;
        stage = "respuesta cita";
		//if (registering) return;
         try {
        if (n[n.length - 1] != undefined && n[n.length - 1].OficinaTexto != undefined && (Administrador.usuario.oficinaTexto = n[n.length - 1].OficinaTexto), i && st(), n[0] != undefined && n[0].Codigo != undefined) Utilidades.agregarErrorDiv("CD", n[0].Descripcion), r && n[0].Codigo === "56" && ($("#divPSedes").collapse("show"), Administrador.irAbajo(), $("#divPDatos").collapse("hide"), $("#divPCitas").collapse("hide"), $("#divPResumen").collapse("hide"), $("#divPMatricula").collapse("hide"));
        else {
            registering = true;
			stage = "cita encontrada";
            // este if es por si la tabla tenia datos entonces los eliminan
            if (Administrador.irArriba(), n.pop(), Utilidades.eliminarErrorDiv("CD"), t.tablaCitas !== undefined && t.tablaCitas !== null) {
               stage = "eliminando tabla";
                var u = $(".ctrl-tabla").parent();
                t.tablaCitas.destruirTabla();
                $("<div class='col-sm-12 ctrl-tabla ctrl-tabla-prueba-practica-minimo-ancho'><\/div>").appendTo(u)
                stage = "tabla eliminada";
            }
            //se agregan las citas a la tabla 
            t.tablaCitas = new Tabla({
                contenedor: $(".ctrl-tabla"),
                datos: ft(n),
                columnas: et(n),
                tipoSeleccion: "celda",
                eventoSeleccionCelda: ot,
                columnaNoSeleccionable: "Fecha",
                filtro: !1
            });
            Utilidades.limpiarErrorDiv()
            seleccionarCita(n);
            stage = "Crea tabla"
            $("#divPCitas").collapse("show");
            $("#divPDatos").collapse("hide");
            $("#divPSedes").collapse("hide");
            $("#divPResumen").collapse("hide");
            $("#divPMatricula").collapse("hide");
            
            
        }
     } catch {
         registering = false;
         startMatricula();
     }
 }
    function falloMatricula() {
        showLog("matricula llego a etapa " + stage + " pero fallo. Recomenzando el ciclo.");
        registering = false;
        startMatricula();
    }

    function seleccionarCita(n) {
        var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth()+1;
        var yyyy = hoy.getFullYear();

        stage = "comprobando cita";
        var d = n[0];

        var ss = "" + d.Fecha;
         
        var fechaC = ss.substr(-10);
        console.log(fechaC);
        var ddC = fechaC.substring(0,2);
        var mmC = fechaC.substring(3,5);
        var yyC = fechaC.substring(6,10);

       console.log ("datos de cita:  dia " + ddC + " mes " + mmC + " anio " + yyC);
       console.log ("datos de Dia actual:  dia " + dd + " mes " + mm + " anio " + yyyy);

       if((mmC == mm || mmC == mm +1) && yyC == yyyy) {
        Administrador.usuario.fechaCita = d.Fecha;
        Administrador.usuario.horaCita = d.Hora;
        $("#fechaCita").text(Administrador.usuario.fechaCita);
        $("#horaCita").text(Administrador.usuario.horaCita);
        $("#divCitaSeleccionada").collapse("show");
        showLog("seleccionando cita");
        resumenMatricula();
        } else {
            if (indicatedTime) falloMatricula();
        }
    }

    function resumenMatricula() {
        stage = "iniciando cargar resumen";
        showLog("cargando resumen matricula");
        try {   
        Administrador.usuario.fechaCita != undefined && Administrador.usuario.fechaCita != null && Administrador.usuario.fechaCita !== "" && Administrador.usuario.horaCita != undefined && Administrador.usuario.horaCita != null && Administrador.usuario.horaCita !== "" ? $("#horaCita").text() != Administrador.usuario.horaCita || $("#fechaCita").text() != Administrador.usuario.fechaCita ? ($("#divCitaSeleccionada").collapse("hide"), $("#horaCita").text(""), $("#fechaCita").text(""), Administrador.usuario.horaCita = null, Administrador.usuario.fechaCita = null, Utilidades.agregarErrorDiv("cargaResumenMatricula", CONSTANTES.Mensajes["008"]), falloMatricula()) : (Utilidades.eliminarErrorDiv("cargaResumenMatricula"), Utilidades.LlamadaAjaxPostJsonAsync({
            funcion: ht,
            url: "/MatriculaPractica/CargarResumenMatricula",
            datos: {
                usuarioObjeto: JSON.stringify(Administrador.usuario),
                rutasObjeto: JSON.stringify(Administrador.rutas)
            }
        })) : Utilidades.agregarErrorDiv("cargaResumenMatricula", CONSTANTES.Mensajes["008"]), falloMatricula() 
    } catch {
        resumenMatricula();
    }
    }

    function ht(n) {
        try {
        if (n[n.length - 1] != undefined && n[n.length - 1].Codigo != undefined && n[n.length - 1].Codigo !== "00") {
            var t = n[n.length - 1].Descripcion;
            n.pop();
            f(n, !0, !0);
            Utilidades.agregarErrorDiv("CD_RM", t);
            Administrador.irAbajo();
            falloMatricula();
        } else Administrador.irArriba(), Utilidades.eliminarErrorDiv("RM"), Utilidades.eliminarErrorDiv("CD_RM"), $("#nombreResumen").val(Administrador.usuario.nombre), $("#tipoIdeResumen").val(Administrador.usuario.tipoIdentificacionTexto), $("#identificacionResumen").val(Administrador.usuario.identificacion), $("#licenciaResumen").val(Administrador.usuario.tipoLicencia), $("#oficinaResumen").val(Administrador.usuario.oficinaTexto), $("#fechaResumen").val(Administrador.usuario.fechaCita), $("#horaResumen").val(Administrador.usuario.horaCita), $("#otrasIndResumen").val(Administrador.usuario.otrasSenas), $("#divPResumen").collapse("show"), $("#divPDatos").collapse("hide"), $("#divPCitas").collapse("hide"), $("#divPSedes").collapse("hide"), $("#divPMatricula").collapse("hide"), Utilidades.limpiarErrorDiv(), matricular()
    } catch {
        resumenMatricula();
    }
    }

    function matricular() {
        showLog("matriculando");
			stage = "iniciando matricula";
			try {
        var n = Administrador.usuario;
        
            Utilidades.LlamadaAjaxPostJsonAsync({
                funcion: ct,
                url: "/MatriculaPractica/MatricularPrueba",
                datos: {
                    usuarioObjeto: JSON.stringify(n),
                    rutasObjeto: JSON.stringify(Administrador.rutas)
                }
            })
        } catch {
            matricular();
        }
        
    }
    function ct(n) {
        try {
        n.Error != undefined && n.Error.Codigo !== "00" ? Utilidades.agregarErrorDiv("MATR", n.Error.Descripcion) : (Administrador.irArriba(), Utilidades.eliminarErrorDiv("MATR"), $("#nombreMatricula").text(Administrador.usuario.nombre), $("#tipoIdeMatricula").text(Administrador.usuario.tipoIdentificacionTexto), $("#identificacionMatricula").text(Administrador.usuario.identificacion), $("#licenciaMatricula").text(Administrador.usuario.tipoLicencia), $("#oficinaMatricula").text(Administrador.usuario.oficinaTexto), $("#fechaMatricula").text(Administrador.usuario.fechaCita), $("#horaMatricula").text(Administrador.usuario.horaCita), $("#otrasIndMatricula").text(Administrador.usuario.otrasSenas), $("#divPMatricula").collapse("show"), $("#divPDatos").collapse("hide"), $("#divPCitas").collapse("hide"), $("#divPSedes").collapse("hide"), $("#divPResumen").collapse("hide"), Utilidades.limpiarErrorDiv(), Administrador.usuario.identificacion = null, Administrador.usuario.llave = null)
        } catch {

            matricular();
        }
    }

    //Funciones requteridas, no se tocan

    function st() {
        Administrador.usuario.fechaCita = null;
        Administrador.usuario.horaCita = null;
        $("#divCitaSeleccionada").collapse("hide");
        $("#horaCita").text("");
        $("#fechaCita").text("")
    }

    function ft(n) {
        var r = [],
            e = [],
            t, u, f, i;
        for (t in n) {
            if (!n.hasOwnProperty(t)) break;
            if (e.indexOf(n[t].Fecha) === -1) u = n[t].Hora, i = {
                Fecha: n[t].Fecha
            }, i[u] = n[t].Disponibles, e.push(n[t].Fecha), r.push(i);
            else {
                u = n[t].Hora;
                f = {};
                for (i in r) r[i].Fecha === n[t].Fecha && (f = r[i]);
                f[u] = n[t].Disponibles
            }
        }
        return r
    }

    function et(n) {
        var r = [],
            t = [],
            i, e, u, f, o;
        for (i in n) {
            if (!n.hasOwnProperty(i)) break;
            t.indexOf(n[i].Hora) === -1 && t.push(n[i].Hora)
        }
        t.sort();
        e = 85 / t.length;
        for (u in t) {
            if (!t.hasOwnProperty(u)) break;
            f = t[u];
            o = {
                codigo: f,
                nombre: f,
                ancho: e + "%"
            };
            r.push(o)
        }
        return r.unshift({
            codigo: "Fecha",
            nombre: "Fecha",
            ancho: "15%"
        }), r
    }
    function ot(n) {
        n != undefined && n.codigoColumna != undefined && n.codigoColumna != null && n.fila != undefined && n.fila != null && n.fila.Fecha != undefined && n.fila.Fecha != null && (Administrador.usuario.fechaCita = n.fila.Fecha, Administrador.usuario.horaCita = n.codigoColumna, $("#fechaCita").text(Administrador.usuario.fechaCita), $("#horaCita").text(Administrador.usuario.horaCita), $("#divCitaSeleccionada").collapse("show"))
    }

    setTimeout(function () {
        showLog("sustituyendo evento");
        var a = $("#botonContinuarSedes");
        a.click(start());
    }, 6000);

    